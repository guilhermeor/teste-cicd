#!/bin/bash

set -e

cd /home/ubuntu/teste-cicd/
git pull
docker build -t guilherme .
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker run -p 8080:80 -d guilherme:latest
curl -X POST --data-urlencode "payload={\"channel\": \"#tim-deploy\", \"username\": \"webhookbot\", \"text\": \"Uoooooooopaaaa!\", \"icon_emoji\": \":ghost:\"}" https://hooks.slack.com/services/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
